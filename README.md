# Ansible - Workshop
Lab 07: Install LAMP Stack

---

# Preperations

## Prepare hosts file - open host file

```
$ sudo vim /etc/ansible/hosts
```

## Paste the following snippet in the file - nodeip will be replaced by the managed node ip(or ip's) - <bold>if the hosts file is already updated, skip this step</bold>

```
[demoservers]
(nodeip(s))
```

## Save and quit
```
press esc
press :wq
```

# Instructions

- Download lamp role
- Define playbook to run the role
- Run the playbook
- Navigate to port 80 to validate the playbook
- Check all components are installed
---

## Download lamp role
```
$ ansible-galaxy install fvarovillodres.lamp
$ cd ~/
```

## Define playbook to run the role

### Create a new playbook file
```
$ sudo vim lampPlaybook.yml
press i
```

### Insert the following snippet
```
---

- hosts: demoservers
  become: true
  roles:
    - role: fvarovillodres.lamp
```
### Save and quit
```
Press esc
Press :wq
```

## Run the playbook
```
$ ansible-playbook lampPlaybook.yml
```

## Check all components are installed
```
$ ansible -a "php -v" demoservers
$ ansible -a "apache2 -v" demoservers
$ ansible -a "mysql --version" demoservers --become

```

## Navigate to port 80 on one of the nodes to check the Apache web server is deployed
```
http://(node-machine-ip):80
```



